from django.shortcuts import render
from django.http import JsonResponse
from .speaker_test import change_volume, inc_volume, dec_volume, mute, unmute, test_speaker

# Create your views here.
def home(request):
    if request.method == "POST":
        if request.POST['action'] == "change":
            change_volume(str(request.POST['volChange']))
            return JsonResponse({'message':'Success'})

        elif request.POST['action'] == "increase":
            inc_volume(str(request.POST['volChangeInc']))
            return JsonResponse({'message':'Success'})

        elif request.POST['action'] == "decrease":
            dec_volume(str(request.POST['volChangeDec']))
            return JsonResponse({'message':'Success'})

        elif request.POST['action'] == "mute":
            mute()
            return JsonResponse({'message':'Success'})

        elif request.POST['action'] == "unmute":
            unmute()
            return JsonResponse({'message':'Success'})

        elif request.POST['action'] == "test":
            test_speaker()
            return JsonResponse({'message':'Success'})

    return render(request, 'index.html')