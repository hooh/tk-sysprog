#!/bin/bash
### BEGIN INIT INFO
#Provides: hoohAutoStart
#Required-Start: $all
#Required-Stop:
#Default-Start: 2 3 4 5
#Default-Stop:
#Short-Description: hoohAutomaticDjangoDeployment
### END INIT INFO
sudo python3 '/home/user/tk-sysprog/WebApp/manage.py' runserver 0.0.0.0:80
echo "Server started."
