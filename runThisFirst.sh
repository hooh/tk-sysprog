#!/bin/bash
#Install dependencies
sudo apt-get install python3 python3-django python3-pip
sudo apt-get install alsa-utils
sudo pip3 install -r '/home/user/tk-sysprog/WebApp/requirements.txt'

#Bootscript
sudo chmod +x '/home/user/tk-sysprog/bootscript/hoohAutoStart.sh'
sudo cp '/home/user/tk-sysprog/bootscript/hoohAutoStart.service' '/etc/systemd/system/'
sudo systemctl daemon-reload
sudo systemctl start hoohAutoStart
sudo systemctl enable hoohAutoStart
sudo shutdown -r 0
