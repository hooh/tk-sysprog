# tksysprog

Tugas Kelompok Sysprog 2020 oleh:
- Abdurrafi Arief (1806205773)
- Mohamad Rifqy Zulkarnaen (1806205552)
- Salman Ahmad Nurhoiriza (1806204890)
___

### Prerequisites:

Configure port forwarding so that the web app that is run at the VM can be accessed from your machine. Just follow the images below.

1. Click on the *Settings* button.

   ![1](/images/1.png)

2. Click on the *Network* tab. Then check the *Enable Network Adapter* checkbox. Then click on *Port Forwarding* button.

   ![2](/images/2.png)

3. Add new rule.

   - Protocol: TCP
   - Host IP: 127.0.0.1
   - Host Port: 80
   - Guest IP: 0.0.0.0
   - Guest Port: 80

   ![3](/images/3.png)

4. Click on OK.

------

### How to run:

1. Clone [this](https://gitlab.com/hooh/tk-sysprog) repository into `/home/user/` on your VM.

2. Goto the repository folder.

   ```
   /home/user$ cd tk-sysprog
   ```

3. Run the `runThisFirst.sh` file. Don't forget to add excecute permission.

   ```
   /home/user/tk-sysprog$ sudo chmod +x runThisFirst.sh
   /home/user/tk-sysprog$ ./runThisFirst.sh
   ```

4. The VM will restart and automatically run the webserver at boot.

5. You can access the webserver at `localhost:80` on your local machine.



